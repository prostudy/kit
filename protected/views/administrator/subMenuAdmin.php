<div class="breadcrumb-env">
						<ol class="breadcrumb bc-1">
							<li class="active">
								<a href="<?php echo Yii::app()->createUrl('Administrator/');?>">
								<i class="fa-wrench"></i>Panel de administración</a>
							</li>
							<li>
								<a href="<?php echo Yii::app()->createUrl('Administrator/adminUsers');?>">
								<i class="fa-users"></i>Lista de usuarios</a>
							</li>
							<li>
								<a href="<?php echo Yii::app()->createUrl('Administrator/adminGenerator');?>">
								<i class="fa-barcode"></i>Generador de códigos NYC</a>							
							</li>
						</ol>
				</div>