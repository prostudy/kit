<div class="page-title">		
	<div class="title-env">
		<h1 class="title">Kit SDGP</h1>
		<p class="description">Bienvenido al Kit para la Implementación del Sistema de Gestión de Datos Personales (SGDP)</p>
	</div>
				
</div>

<div class="row">
	<div class="col-md-12">				
		<!-- Colored panel, remember to add "panel-color" before applying the color -->
		<div class="panel panel-color panel-gray"><!-- Add class "collapsed" to minimize the panel -->
			<div class="panel-heading">
				<h3 class="panel-title">¿Quiénes Somos?</h3>
			</div>
			
			<div class="panel-body">
				<h3>&nbsp;<strong>Introducci&oacute;n al KIT SGDP</strong></h3>
				
				<p>Esta herramienta est&aacute; dirigida a todas aquellas personas f&iacute;sicas o morales que venden bienes o prestan alg&uacute;n servicio dentro sector privado y que pueden llegar a ser responsables del tratamiento de datos personales, independientemente del ramo en el que se desarrollen, as&iacute; como a los encargados y terceros y tiene como finalidad que el usuario, a&uacute;n sin tener conocimiento alguno del contenido de la Ley de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de los Particulares (LFPDPPP), pueda realizar un blindaje dentro de su organizaci&oacute;n, de modo tal que los datos que maneja sean pertinentes, correctos y actualizados, seg&uacute;n lo establecido por la Ley mencionada.</p>
				
				<p>Es una herramienta que te permitir&aacute; cumplir con la LPDPPP, logrando:</p>
				<br>
				<ol>
				    <li>Entender qu&eacute; son los datos personales.</li>
				    <li>Distinguir los datos personales de los datos sensibles, as&iacute; como de otro tipo de informaci&oacute;n.</li>
				    <li>Conocer cu&aacute;les son tus obligaciones como responsable de los datos personales con los que cuentas.</li>
				    <li>Ser capaz de redactar tu propio Aviso de Privacidad, de acuerdo al sector de la industria o comercio en se encuentra tu campo de desarrollo.</li>
				    <li>Entender c&oacute;mo conformar tu Departamento de Datos Personales, que sea adecuado al perfil de tu organizaci&oacute;n y que est&eacute; lo suficientemente capacitado para cumplir con la Ley, tanto de manera preventiva, como ante una solicitud de ejercicio de derechos ARCO y frente a requerimientos o visitas de verificaci&oacute;n del INAI.</li>
				    <li>Definir el volumen de datos personales que manejas y evaluar si est&aacute;n protegidos correctamente.</li>
				    <li>Tomar las medidas de seguridad correspondientes para proteger los datos personales que manejas.</li>
				    <li>Tener el suficiente conocimiento para implementar un mecanismo de autoevaluaci&oacute;n continua y de este modo estar siempre en cumplimiento de la normatividad de Protecci&oacute;n de Datos Personales.</li>
				    <li>Implementar un Sistema de gesti&oacute;n de datos personas de acuerdo a los Par&aacute;metros de autorregulaci&oacute;n en materia de protecci&oacute;n de datos personales.</li>
				    <li>Alinear el Sistema de gesti&oacute;n de datos personales a un Esquema de autorregulaci&oacute;n certificable.</li>
				</ol>
				
				<h3><strong>Estructura del Kit SGDP</strong></h3>
				
				<p>El Kit SGDP est&aacute; integrado por distintos elementos, mismos que conforman un conjunto de herramientas relacionadas con seguridad de la informaci&oacute;n y protecci&oacute;n de datos personales que los usuarios al interior de las empresas podr&aacute;n utilizar de manera sencilla.</p>
				
				<p>Cada elemento dentro del Kit es f&aacute;cilmente localizable debido al esquema de referencia que se muestra a continuaci&oacute;n:</p>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">	
	<div class="panel panel-flat"><!-- Add class "collapsed" to minimize the panel -->
						<div class="panel-heading">
							<h3 class="panel-title">Módulos</h3>
						</div>
						
						<div class="panel-body">
							
								<center><img src="http://pmstudykit.com/kitsgdp/images/mainImageToolkit.png" alt="" width="70%" /></center>
						
						</div>
					</div>			
	</div>
	
</div>

	
<script type="text/javascript">
				var sample_notification;
				
					jQuery(document).ready(function($)
					{	
							
					// Notifications
					window.clearTimeout(sample_notification);
					
					var notification = setTimeout(function()
					{			
						var variablejs = "<?php echo Yii::app()->session['messageRestricted']; ?>" ;
						
						var opts = {
								"closeButton": true,
								"debug": false,
								"positionClass": "toast-bottom-left",
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"timeOut": "5000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut"
							};
				
						toastr.warning(variablejs,null, opts);
					}, 3800);
					
					});
			</script>